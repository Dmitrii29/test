package ru.ok.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ru.ok.data.UserData;
import ru.ok.utils.ConfigProperties;

public class LoginPage extends Page {

	@FindBy(linkText = "form.form > div.form-actions > input.button-pro.form-actions_yes")
	public WebElement linkSignIn;
	
	@FindBy(id = "field_email")
	public WebElement fieldUsername;

	@FindBy(id = "field_password")
	public WebElement fieldPassword;

	@FindBy(name = "form.form > div.form-actions > input.button-pro.form-actions_yes")
	public WebElement buttonLogin;
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public HomePage loginAs(UserData admin) {
		linkSignIn.click();
		type(fieldUsername, admin.name);
		type(fieldPassword, admin.password);
		buttonLogin.click();
		return PageFactory.initElements(driver, HomePage.class);
	}

	@Override
	public void open() {
		driver.get(ConfigProperties.getProperty("login.url"));
	}
	

}
