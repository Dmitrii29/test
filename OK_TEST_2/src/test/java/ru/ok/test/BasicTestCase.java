package ru.ok.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

import ru.ok.data.UserData;
import ru.ok.utils.ConfigProperties;

public class BasicTestCase {

	protected static WebDriver driver;
	
	public UserData admin = new UserData("mitrofan29", "dawkevich1992");

	protected WebDriver getWebDriver() {
		if (driver == null) {
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(Long.parseLong(ConfigProperties.getProperty("imp.wait")), TimeUnit.SECONDS);
		}
		return driver;
	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	
}
